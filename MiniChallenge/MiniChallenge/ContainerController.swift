//
//  ContainerController.swift
//  MiniChallenge
//
//  Created by Eldade Marcelino on 16/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import UIKit

class ContainerController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var invisibleButton: UIButton!
    private var containerViewFrameBackup = CGRect()
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuFrame: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.containerViewFrameBackup = self.containerView.frame
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showMenu(_ sender: UIButton) {
       
        UIView.animate(withDuration: 0.3, animations: {
            self.containerView.transform = CGAffineTransform(scaleX: 0.85,y: 0.85);
            let frame = self.containerView.frame
            self.containerView.frame = CGRect(x: self.view.frame.width * 0.7, y: frame.minY, width: frame.width, height: frame.height)
            self.invisibleButton.frame = self.containerView.frame
            self.view.backgroundColor = self.menuView.backgroundColor
        }) { (_) in
            self.containerView.layer.shadowOpacity = 1
        }
    }
    
    @IBAction func restoreView(_ sender: Any) {
        self.containerView.layer.shadowOpacity = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.containerView.transform = CGAffineTransform(scaleX: 1.0,y: 1.0);
            self.containerView.frame = self.containerViewFrameBackup
            self.invisibleButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.view.backgroundColor = self.menuFrame.backgroundColor
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
