//
//  MainScreenViewModel.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import Foundation
import UIKit

public class MainScreenViewModel {
    
    class Card {
        
        var title: String = ""
        var image: UIImage = UIImage()
        
    }
    
    var cards = [Card]()
    
    class EventsView: MainScreenViewModel {
        
    }
    
    class InstitutionsView: MainScreenViewModel {
        
    }
}
