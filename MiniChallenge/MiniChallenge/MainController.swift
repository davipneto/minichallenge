//
//  ViewController.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import UIKit
import TextFieldEffects

class MainController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let next = self.storyboard?.instantiateViewController(withIdentifier: "bacon")
        self.present(next!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

