//
//  File.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import Foundation
import UIKit

public class User {
    
    var name: String
    var birth: Date
    var rg: String
    var cpf: String
    var location: Location?
    var email: String
    var picture: URL?
    
    init(name: String, birth: Date, rg: String, cpf: String, email: String) {
        
        self.name = name
        self.birth = birth
        self.rg = rg
        self.cpf = cpf
        self.email = email
    
    }
    
}
