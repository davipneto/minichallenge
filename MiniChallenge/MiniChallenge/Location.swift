//
//  Location.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import Foundation

public class Location {
    
    var streetName: String
    var streetNumber: Int
    var neighborhood: String
    var city: String
    var state: String
    var country: String
    var latitude: Double
    var longitude: Double
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        self.streetName = ""
        self.streetNumber = 0
        self.neighborhood = ""
        self.city = ""
        self.state = ""
        self.country = "Brazil"
    }
    
}
