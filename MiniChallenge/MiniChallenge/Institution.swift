//
//  Institution.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import Foundation

public class Institution {
    
    var name: String
    var cnpj: String
    var location: Location?
    var phoneNumber: String?
    var webPage: URL?
    var about: String?
    var logo: URL
    var email: String
    var events = [Event]()
    
    init(name: String, cnpj: String, logo: URL, email: String) {
        
        self.name = name
        self.cnpj = cnpj
        self.logo = logo
        self.email = email
        
    }
    
}
