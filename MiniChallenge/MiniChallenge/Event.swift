//
//  Event.swift
//  MiniChallenge
//
//  Created by Davi Pereira Neto on 11/05/17.
//  Copyright © 2017 Davi Pereira Neto. All rights reserved.
//

import Foundation

public class Event {
    
    var title: String
    var location: Location?
    var institution: Institution
    var users = [User]()
    var dateStart: Date
    var dateEnd: Date?
    
    init(title: String, institution: Institution, dateStart: Date) {
        
        self.title = title
        self.institution = institution
        self.dateStart = dateStart
        
    }
    
}
